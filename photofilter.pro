#-------------------------------------------------
#
# Project created by QtCreator 2016-04-27T12:13:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = photofilter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    colors.cpp

HEADERS  += mainwindow.h \
    colors.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11 -U__STRICT_ANSI__

RESOURCES += \
    photofilter.qrc

#ifndef COLORS_H
#define COLORS_H

#include <math.h>
//---------------------------------------------------------------------------
void RGB2HSV(double r,double g,double b,double &h,double &s,double &v);
void HSV2RGB(double h,double s,double v,double &r,double &g,double &b);

void YUV2RGB(int y,int u,int v,int &r,int &g,int &b);
void RGB2YUV(int r,int g,int b,int &y,int &u,int &v);

#endif // COLORS_H

#include "colors.h"
//---------------------------------------------------------------------------

void RGB2HSV(double r,double g,double b,double &h,double &s,double &v)
{
        double cmin;
        double cmax;
        double delta;
        if(r<=g){
                if(r<=b){
                        cmin = r;
                }
                else{
                        cmin = b;
                }
        }
        else{
                if(g<=b){
                        cmin = g;
                }
                else{
                        cmin = b;
                }
        }

        if(r>=g){
                if(r>=b){
                        cmax = r;
                }
                else{
                        cmax = b;
                }
        }
        else{
                if(g>=b){
                        cmax = g;
                }
                else{
                        cmax = b;
                }
        }

        delta = cmax-cmin;
        if((v=cmax)!=0){
                s=delta/cmax;
        }
        else{
                s=0;
        }
        if(s == 0){h=0;}
        else{
        if(r==v){
                h=(g-b)/delta;
        }
        else{
                if(g==v){
                        h=2+(b-r)/delta;
                }
                else{
                        h=4+(r-g)/delta;
                }
        }
        if((h*=60)<0) h+=360;
        }
}
//---------------------------------------------------------------------------

void HSV2RGB(double h,double s,double v,double &r,double &g,double &b)
{
        if(s==0){
                r=v;
                g=v;
                b=v;
        }
        else{
                if(h==360)h=0;
                h=h/60;
                int i= floor(h);
                double f=h-i;
                double p=v*(1.0-s);
                double q=v*(1.0-s*f);
                double t=v*(1.0-s*(1.0-f));
                switch(i){
                        case 0:
                                r=v;
                                g=t;
                                b=p;
                                break;
                        case 1:
                                r=q;
                                g=v;
                                b=p;
                                break;
                        case 2:
                                r=p;
                                g=v;
                                b=t;
                                break;
                        case 3:
                                r=p;
                                g=q;
                                b=v;
                                break;
                        case 4:
                                r=t;
                                g=p;
                                b=v;
                                break;
                        case 6:
                                r=v;
                                g=p;
                                b=q;
                                break;
                }
        }
}

void YUV2RGB(int y,int u,int v,int &r,int &g,int &b)
{
    r = y + 1.13983*(v);
    g = y - 0.39465*(u) - 0.58060*(v);
    b = y + 2.03211*(u);
    if(r<=0)r=0;
    if(r>=255)r=255;
    if(g<=0)g=0;
    if(g>=255)g=255;
    if(b<=0)b=0;
    if(b>=255)b=255;
}

void RGB2YUV(int r,int g,int b,int &y,int &u,int &v)
{
    y = 0.299*r + 0.587*g + 0.114*b;
    u = -0.14713*r - 0.28886*g + 0.436*b;// + 128;
    v = 0.615*r - 0.51499 * g - 0.10001*b;// + 128;
}

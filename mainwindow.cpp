#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
//open graphic file
void MainWindow::on_OpenAction_triggered()
{
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("Jpeg file (*.jpg)"));
    QPixmap pix;
    Originimage = new QImage(FileName);
    pix.load(FileName);
    ui->OriginalImage->setPixmap(pix);
}
//Gauss blur
void MainWindow::on_GaussBlurAction_triggered()
{
    ui->ResultImage->clear();
    int R;
    double sigma;
    bool ok;
    sigma = QInputDialog::getInt(this,"Gauss blur","radius",0,0,100,1,&ok);
    if(!ok)return;
    R = ceil(3*sigma);
    double *Gauss;
    Gauss = new double [2*R+1];
    int i,j;
    double gausssumm;
    gausssumm = 0;
    for(i=0;i<2*R+1;i++){
         Gauss[i] = 1.0/sqrt(2.0*M_PI)/sigma*exp(-(double)(i-R)*(i-R)/(2.0*sigma*sigma));
         gausssumm = gausssumm+Gauss[i];
         qDebug() << Gauss[i];
    }
    qDebug() << gausssumm;
    for(i=0;i<2*R+1;i++){
        Gauss[i] = Gauss[i]/gausssumm;
    }

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    int r1,g1,b1;
    double r2,g2,b2;
    QRgb rgb;
    int k,ind;
    QImage resimage(M,N,QImage::Format_RGB32);
    QImage resimage2(M,N,QImage::Format_RGB32);
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            r2 = 0;
            g2 = 0;
            b2 = 0;
            for(k=0;k<2*R+1;k++){
                ind = j+k-R;
                if(j+k-R<0){
                    ind = 0;
                }
                if(j+k-R>=N-1){
                    ind = N-1;
                }
                rgb = Originimage->pixel(i,ind);
                r1 = qRed(rgb);
                g1 = qGreen(rgb);
                b1 = qBlue(rgb);
                r2 = r2+r1*Gauss[k];
                g2 = g2+g1*Gauss[k];
                b2 = b2+b1*Gauss[k];
            }
            resimage.setPixel(i,j,qRgb(ceil(r2),ceil(g2),ceil(b2)));
        }
    }
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            r2 = 0;
            g2 = 0;
            b2 = 0;
            for(k=0;k<2*R+1;k++){
                ind = i+k-R;
                if(i+k-R<0){
                    ind = 0;
                }
                if(i+k-R>=M-1){
                    ind = M-1;
                }
                rgb = resimage.pixel(ind,j);
                r1 = qRed(rgb);
                g1 = qGreen(rgb);
                b1 = qBlue(rgb);
                r2 = r2+r1*Gauss[k];
                g2 = g2+g1*Gauss[k];
                b2 = b2+b1*Gauss[k];
            }
            resimage2.setPixel(i,j,qRgb(r2,g2,b2));
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage2);
    ui->ResultImage->setPixmap(pix);
}
//save file
void MainWindow::on_SaveAction_triggered()
{
    QString FileName = QFileDialog::getSaveFileName(this, tr("Save File"),"",tr("Jpeg file (*.jpg)"));
    ui->ResultImage->pixmap()->save(FileName);
}
//sub image
void MainWindow::on_SubImageAction_triggered()
{
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("Jpeg file (*.jpg)"));
    //QPixmap pix;
    QImage image = QImage(FileName);
    //pix.load(FileName);
    //ui->OriginalImage->setPixmap(pix);
    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    int R1,G1,B1;
    int R2,G2,B2;
    QRgb rgb1,rgb2;
    int i,j;
    QImage resimage(M,N,QImage::Format_RGB32);
    double error;
    error = 0;
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            rgb1 = Originimage->pixel(i,j);
            rgb2 = image.pixel(i,j);
            R1 = qRed(rgb1);
            G1 = qGreen(rgb1);
            B1 = qBlue(rgb1);
            R2 = qRed(rgb2);
            G2 = qGreen(rgb2);
            B2 = qBlue(rgb2);

            resimage.setPixel(i,j,qRgb(R2-R1,G2-G1,B2-B1));

            if(error < (R2-R1)*(R2-R1)+(G2-G1)*(G2-G1)+(B2-B1)*(B2-B1)){
                error = (R2-R1)*(R2-R1)+(G2-G1)*(G2-G1)+(B2-B1)*(B2-B1);
            }
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
    QMessageBox message;
    message.setText(QString("%1").arg(sqrt(error)));
    message.exec();
}
//median filter
void MainWindow::on_MedianAction_triggered()
{
    int R;
    bool ok;
    R = QInputDialog::getInt(this,"median filter","radius",0,0,100,1,&ok);
    if(!ok)return;
    double *Median;
    Median = new double [R*R];
    int i,j;

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    QRgb rgb;
    int k1,k2,indi,indj;
    QImage resimage(M,N,QImage::Format_RGB32);
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            QList <QColor> list;
            list.clear();
            for(k1=0;k1<2*R+1;k1++){
                for(k2=0;k2<2*R+1;k2++){
                    indi = i+k1-R;
                    indj = j+k2-R;
                    if(i+k1-R<0){
                        indi = 0;
                    }
                    if(i+k1-R>=M-1){
                        indi = M-1;
                    }
                    if(j+k2-R<0){
                        indj = 0;
                    }
                    if(j+k2-R>=N-1){
                        indj = N-1;
                    }
                    rgb = Originimage->pixel(indi,indj);
                    list.push_back(QColor(rgb));
                }
            }
            qSort(list.begin(), list.end(), [](QColor a, QColor b) { return (a.red()*a.red()) <= (b.red()*b.red()); } );
            resimage.setPixel(i,j,list[list.size()/2].rgb());
            //(qRed(a)*qRed(a)+qGreen(a)*qGreen(a)+qBlue(a)*qBlue(a)) <= (qRed(b)*qRed(b)+qGreen(b)*qGreen(b)+qBlue(b)*qBlue(b))
        }
        QCoreApplication::processEvents();
    }

    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
}
//Brightness
void MainWindow::on_BrightnessAction_triggered()
{
    int Brightness;
    bool ok;
    Brightness = QInputDialog::getInt(this,"Brightness","%",100,0,200,1,&ok);
    if(!ok)return;

    int i,j;

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    QRgb rgb;
    //QColor color;
    double h,s,v;
    double r,g,b;
    int R,G,B;
    int Y,U,V;
    QImage resimage(M,N,QImage::Format_RGB32);
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            rgb = Originimage->pixel(i,j);
            r = (double)qRed(rgb)/255;
            g = (double)qGreen(rgb)/255;
            b = (double)qBlue(rgb)/255;
            RGB2HSV(r,g,b,h,s,v);
            v = v*Brightness/100;
            //s = s*(200-Brightness)/100;
            //if(s>=1.0)s=1.0;
            if(v>=1.0)v=1.0;
            HSV2RGB(h,s,v,r,g,b);
            resimage.setPixel(i,j,qRgb(r*255,g*255,b*255));
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
}

void MainWindow::on_BrightnessAction2_triggered()
{
    int Brightness;
    bool ok;
    Brightness = QInputDialog::getInt(this,"Brightness","%",100,0,200,1,&ok);
    if(!ok)return;

    int i,j;

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    QRgb rgb;
    //QColor color;
    double h,s,v;
    double r,g,b;
    int R,G,B;
    int Y,U,V;
    QImage resimage(M,N,QImage::Format_RGB32);
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            rgb = Originimage->pixel(i,j);
            R = qRed(rgb);
            G = qGreen(rgb);
            B = qBlue(rgb);
            RGB2YUV(R,G,B,Y,U,V);
            Y = (Y*Brightness/100);
            if(Y>256)Y = 256;
            YUV2RGB(Y,U,V,R,G,B);
            resimage.setPixel(i,j,qRgb(R,G,B));
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
}

void MainWindow::on_ContrastAction_triggered()
{
    int Contrast;
    bool ok;
    Contrast = QInputDialog::getInt(this,"contrast","%",100,0,200,1,&ok);
    if(!ok)return;

    int i,j;

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    QRgb rgb;
    double h,s,v;
    double r,g,b;
    int R,G,B;
    int Y,U,V;
    QImage resimage(M,N,QImage::Format_RGB32);
    double Ymean;
    Ymean = 0;
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            rgb = Originimage->pixel(i,j);
            R = qRed(rgb);
            G = qGreen(rgb);
            B = qBlue(rgb);
            Ymean = Ymean+0.3 * R + 0.59 * G + 0.11 * B;
        }
    }
    Ymean = Ymean/M/N;
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            rgb = Originimage->pixel(i,j);
            R = qRed(rgb);
            G = qGreen(rgb);
            B = qBlue(rgb);
            R = Ymean+((double)Contrast/100)*(R-Ymean);
            G = Ymean+((double)Contrast/100)*(G-Ymean);
            B = Ymean+((double)Contrast/100)*(B-Ymean);
            if(R>256)R = 255;
            if(G>256)G = 255;
            if(B>256)B = 255;
            resimage.setPixel(i,j,qRgb(R,G,B));
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
}

void MainWindow::on_SaturationAction_triggered()
{
    int Saturation;
    bool ok;
    Saturation = QInputDialog::getInt(this,"brightness","%",100,0,200,1,&ok);
    if(!ok)return;

    int i,j;

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    QRgb rgb;
    //QColor color;
    double h,s,v;
    double r,g,b;
    int R,G,B;
    int Y,U,V;
    QImage resimage(M,N,QImage::Format_RGB32);
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            rgb = Originimage->pixel(i,j);
            r = (double)qRed(rgb)/255;
            g = (double)qGreen(rgb)/255;
            b = (double)qBlue(rgb)/255;
            RGB2HSV(r,g,b,h,s,v);
            //v = v*Brightness/100;
            s = s*Saturation/100;
            if(s>=1.0)s=1.0;
            //if(v>=1.0)v=1.0;
            HSV2RGB(h,s,v,r,g,b);
            resimage.setPixel(i,j,qRgb(r*255,g*255,b*255));
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
}

void MainWindow::on_GammaCorrectionAction_triggered()
{
    double gamma;
    bool ok;
    gamma = QInputDialog::getDouble(this,"gamma","value",1.0,0.0,10.0,1,&ok);
    if(!ok)return;

    int i,j;

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    QRgb rgb;
    //QColor color;
    double h,s,v;
    double r,g,b;
    int R,G,B;
    int Y,U,V;
    QImage resimage(M,N,QImage::Format_RGB32);
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            rgb = Originimage->pixel(i,j);
            r = (double)qRed(rgb)/255;
            g = (double)qGreen(rgb)/255;
            b = (double)qBlue(rgb)/255;
            r = pow(r,gamma);
            g = pow(g,gamma);
            b = pow(b,gamma);
            resimage.setPixel(i,j,qRgb(r*255,g*255,b*255));
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
}

void MainWindow::on_BluehorizontalSlider_sliderMoved(int position)
{
    double gamma;
    //bool ok;
    gamma = 1.0+(double)position/10.0;
    int i,j;

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    QRgb rgb;
    double h,s,v;
    double r,g,b;
    int R,G,B;
    int Y,U,V;
    QImage resimage(M,N,QImage::Format_RGB32);
    for(i=0;i<M;i++){
        for(j=0;j<N;j++){
            rgb = Originimage->pixel(i,j);
            r = (double)qRed(rgb)/255;
            g = (double)qGreen(rgb)/255;
            b = (double)qBlue(rgb)/255;
            r = pow(r,gamma);
            g = pow(g,gamma);
            b = pow(b,gamma);
            resimage.setPixel(i,j,qRgb(r*255,g*255,b*255));
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
}

void MainWindow::on_PixilizationAction_triggered()
{
    int delta;
    bool ok;
    delta = QInputDialog::getInt(this,"delta","delta",1,1,1000,1,&ok);
    if(!ok)return;

    int i,j;
    int ii,jj;

    int N,M;
    M = Originimage->width();
    N = Originimage->height();
    QRgb rgb;
    int r,g,b;
    int R,G,B;
    int s;
    QImage resimage(M,N,QImage::Format_RGB32);

    //QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("All file (*.*)"));
    //QImage ImageMask = QImage(FileName).scaled(delta,delta);
    QImage ImageMask = QImage(":image/lego.jpg").scaled(delta,delta);
    for(i=0;i<M/delta+1;i++){
        for(j=0;j<N/delta+1;j++){
            r = 0;
            g = 0;
            b = 0;
            s = 0;
            for(ii=0;ii<delta;ii++){
                for(jj=0;jj<delta;jj++){
                    if(j*delta+jj>=N){
                        continue;
                    }
                    if(i*delta+ii>=M)continue;
                    rgb = Originimage->pixel(i*delta+ii,j*delta+jj);
                    s++;
                    r += qRed(rgb);
                    g += qGreen(rgb);
                    b += qBlue(rgb);
                }
            }
            if(i == M/delta-1){
                int a = 0;
            }
            for(ii=0;ii<delta;ii++){
                for(jj=0;jj<delta;jj++){
                    if(i*delta+ii>=M)continue;
                    if(j*delta+jj>=N)continue;
                    rgb = ImageMask.pixel(ii,jj);
                    R = qRed(rgb);
                    G = qGreen(rgb);
                    B = qBlue(rgb);
                    resimage.setPixel(i*delta+ii,j*delta+jj,qRgb((r/s+R)/2,(g/s+G)/2,(b/s+B)/2));
                }
            }
        }
    }
    QPixmap pix(M,N);
    pix = QPixmap::fromImage(resimage);
    ui->ResultImage->setPixmap(pix);
}

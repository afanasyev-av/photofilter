#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QDebug>
#define _USE_MATH_DEFINES
#include <cmath>
#include "colors.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_OpenAction_triggered();

    void on_GaussBlurAction_triggered();

    void on_SaveAction_triggered();

    void on_SubImageAction_triggered();

    void on_MedianAction_triggered();

    void on_BrightnessAction_triggered();

    void on_BrightnessAction2_triggered();

    void on_ContrastAction_triggered();

    void on_SaturationAction_triggered();

    void on_GammaCorrectionAction_triggered();

    void on_BluehorizontalSlider_sliderMoved(int position);

    void on_PixilizationAction_triggered();

private:
    Ui::MainWindow *ui;
    QImage *Originimage;
};

#endif // MAINWINDOW_H
